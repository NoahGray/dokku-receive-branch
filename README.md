# dokku-receive-branch

Plugin which allows you to use multiple branches with dokku

# Install

```bash
sudo dokku plugin:install https://github.com/cinarra/dokku-receive-branch.git
```

# Update

```bash
sudo dokku plugin:update receive-branch
```
