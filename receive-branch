#!/usr/bin/env bash
set -eo pipefail; [[ $DOKKU_TRACE ]] && set -x
# Gives dokku the ability to support multiple branches for a given service
# Allowing you to have multiple staging environments on a per-branch basis

reference_app=$1
refname=$3
newrev=$2
APP=${refname/*\//}.$reference_app

get_global_vhost() {
  declare desc="return global vhost"
  local GLOBAL_VHOST_FILE="$DOKKU_ROOT/VHOST"
  [[ -f "$GLOBAL_VHOST_FILE" ]] && local GLOBAL_VHOST=$(< "$GLOBAL_VHOST_FILE")
  echo "$GLOBAL_VHOST"
}

if [[ ! -d "$DOKKU_ROOT/$APP" ]]; then
  # Don't create repo on destroy command
  if [[ $newrev != "0000000000000000000000000000000000000000" ]]; then
    # every app needs its own folder, create shared reference repo
    # if main app is not exists this will fail so always create first deploy on master
    REFERENCE_REPO="$DOKKU_ROOT/$reference_app"
    git clone --bare --shared --reference "$REFERENCE_REPO" "$REFERENCE_REPO" "$DOKKU_ROOT/$APP" > /dev/null
    # create APP VHOST entry (default is broken)
    GLOBAL_VHOST=$(get_global_vhost)
    echo "HOSTNAME IS ${APP}.${GLOBAL_VHOST}"
    echo "${APP}.${GLOBAL_VHOST}" > "$DOKKU_ROOT/$APP/VHOST"
  fi
else
  if [[ $newrev = "0000000000000000000000000000000000000000" ]]; then
    # branch remove command received
    echo "START DESTROYING APP $APP"
    dokku apps:destroy "$APP" force
  fi
fi

if [[ $newrev != "0000000000000000000000000000000000000000" ]]; then
  plugn trigger receive-app $APP $newrev
fi
